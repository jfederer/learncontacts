'use strict';

describe('Controller: AboutCtrl', function () {

  // load the controller's module
  beforeEach(module('learnContactsApp'));

  var AboutCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AboutCtrl = $controller('AboutCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of aboutThings to the scope with length of 3', function () {
    expect(scope.aboutThings.length).toBe(3);
  });
});
