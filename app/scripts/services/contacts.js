'use strict';
var LCAApp = angular.module('learnContactsApp');

/**
 * @ngdoc function
 * @name learnContactsApp.controller:ContactsCtrl
 * @description
 * # ContactsCtrl
 * Controller of the learnContactsApp
*/

LCAApp.factory('PeopleListService', function() {
    var fact = {};
  var peopleList = [
        'Joe',
        'Adam',
        'Rob',
        'Corbin'
      ];

    fact.getPeople = function () {
      return peopleList;
    };

    fact.addPerson = function(name) {
    if (peopleList.indexOf(name)===-1) {
      peopleList.push(name);
      return fact.getPeople();
    } else {
      throw 'Name (' + name + ') already in awesomePeople list';
    }
  };

  fact.deletePerson = function(name) { 
    if (peopleList.indexOf(name)!==-1) {
      peopleList.splice(peopleList.indexOf(name),1);
      return fact.getPeople();
    } else {
      throw 'Name to delete (' + name + ') not in awesomePeople list';
    }
  };
      return fact;
    }); 
	
