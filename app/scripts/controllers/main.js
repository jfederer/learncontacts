'use strict';

/**
 * @ngdoc function
 * @name learnContactsApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the learnContactsApp
 */
angular.module('learnContactsApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
