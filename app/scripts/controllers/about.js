'use strict';

/**
 * @ngdoc function
 * @name learnContactsApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the learnContactsApp
 */
angular.module('learnContactsApp')
	.controller('AboutCtrl', function ($scope) {
		$scope.aboutThings = [
			'34 years old',
			'Father',
			'Frustrated'
		];
	});
