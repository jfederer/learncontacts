'use strict';
var LCAApp = angular.module('learnContactsApp');
/**
 * @ngdoc function
 * @name learnContactsApp.controller:ContactsCtrl
 * @description
 * # ContactsCtrl
 * Controller of the learnContactsApp
 */

	
LCAApp.controller('ContactsCtrl', ['$scope', 'PeopleListService', function ($scope, PeopleListService) {
	$scope.awesomePeople = PeopleListService.getPeople();
	$scope.addPerson = function (name) {
		try {
			$scope.awesomePeople = PeopleListService.addPerson(name);
		}
		catch(err) {
			throw err;
		}
	};
	$scope.deletePerson = function(name) {
		try {
			$scope.awesomePeople = PeopleListService.deletePerson(name);
		}
		catch(err) {
			throw err;
		}
	};
}]);


